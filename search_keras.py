
from extract_cnn_vgg16_keras import VGGNet
from keras import backend as K
import numpy as np
import h5py


def test(queryDir):
    # Before prediction
    K.clear_session()
    h5f = h5py.File('./featureCNN_augmented.h5', 'r')
    feats = h5f['dataset_feat'][:]
    imgNames = h5f['dataset_name'][:]
    h5f.close()

    # init VGGNet16 model
    model = VGGNet()

    # extract query image's feature, compute simlarity score and sort
    queryVec = model.extract_feat(queryDir)
    scores = np.dot(queryVec, feats.T)
    rank_ID = np.argsort(scores)[::-1]
    rank_score = scores[rank_ID]

    # After prediction
    K.clear_session()

    maxres = 3
    imlist = list()
    scores = list(rank_score[0:maxres])
    paths = list(rank_ID[0:maxres])

    added_img_names = list()

    i = 0
    n = 0
    while n < maxres:
        img_name = imgNames[rank_ID[i]].decode('utf-8')
        if img_name not in added_img_names:
            n += 1
            added_img_names.append(img_name)
            result = dict()
            result['path'] = '/dataset-retr/train/' + img_name
            result['score'] = rank_score[i]
            imlist.append(result)
        i += 1

    return imlist
