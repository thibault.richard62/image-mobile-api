
import os
import h5py
import numpy as np

from extract_cnn_vgg16_keras import VGGNet
'''
 Extract features and index the images
'''
'''
 Returns a list of filenames for all jpg images in a directory. 
'''
def get_imlist(path):
    return [os.path.join(path,f) for f in os.listdir(path) if f.endswith('.jpg')]

if __name__ == "__main__":

    db = img_paths = 'dataset-retr/train'
    img_list = get_imlist(db)
    
    print("--------------------------------------------------")
    print("         feature extraction starts")
    print("--------------------------------------------------")
    
    feats = []
    names = []

    model = VGGNet()
    for i, img_path in enumerate(img_list):
        norm_feats = model.extract_feat_with_augmentation(img_path)
        img_name = os.path.split(img_path)[1]
        for feat in norm_feats:
            feats.append(feat)
            names.append(img_name.encode('utf-8'))
        #print(img_path, img_name)
        print("extracting feature from image No. %d , %d images in total" %((i+1), len(img_list)))

    #feats = np.array(feats)
    output = 'featureCNN_augmented.h5'

    print(names)
    
    print("--------------------------------------------------")
    print("      writing feature extraction result ...")
    print("--------------------------------------------------")
    
    h5f = h5py.File(output, 'w')
    h5f.create_dataset('dataset_feat', data=feats)
    h5f.create_dataset('dataset_name', data=names)
    h5f.close()
