from django.db import models

class File(models.Model):
    file = models.FileField(blank=False, null=False)
    # PARTIE 1
    mail = models.CharField(blank=True, null=True, max_length=50)
    comment = models.TextField(blank=True, null=True)
    def __str__(self):
        return self.file.name

# PARTIE 2
class Rate(models.Model):
    matched_image = models.TextField(blank=False, null=False)
    sent_image = models.ForeignKey(File, on_delete=models.CASCADE)
    rate = models.IntegerField(blank=False, null=True)
    def __str__(self):
        return self.rate.name