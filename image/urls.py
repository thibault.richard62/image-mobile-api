from django.urls import path
from .views import *

urlpatterns = [
    path('searches', FileUploadView.as_view()),
    path('submit', FileSubmitView.as_view()),
    path('rate', FileRateView.as_view())
]