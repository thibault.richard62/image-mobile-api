from rest_framework.parsers import FileUploadParser
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework import status
from search_keras import test

from .serializers import FileSerializer
from .serializers import RateSerializer


class FileUploadView(APIView):
    parser_class = (FileUploadParser,)

    def post(self, request, *args, **kwargs):
      file_serializer = FileSerializer(data=request.data)
      print(request.data)

      if file_serializer.is_valid():
          file_serializer.save()
          data = file_serializer.data
          data['match'] = test('.' + file_serializer.data['file'])
          return Response(data, status=status.HTTP_201_CREATED)
      else:
          return Response(file_serializer.errors, status=status.HTTP_400_BAD_REQUEST)

# PARTIE 1
class FileSubmitView(APIView):
    parser_class = (FileUploadParser,)

    def post(self, request, *args, **kwargs):
      file_serializer = FileSerializer(data=request.data)

      if file_serializer.is_valid():
          file_serializer.save()
          data = file_serializer.data
          return Response(data, status=status.HTTP_201_CREATED)
      else:
          return Response(file_serializer.errors, status=status.HTTP_400_BAD_REQUEST)

# PARTIE 2
class FileRateView(APIView):

    def post(self, request, *args, **kwargs):
        print(request.body)
        rate_serializer = RateSerializer(data=request.data)

        print(request.data)

        if rate_serializer.is_valid():
            rate_serializer.save()
            data = rate_serializer.data
            print(rate_serializer.data)

            return Response(data, status=status.HTTP_201_CREATED)
        else:
            return Response(rate_serializer.errors, status=status.HTTP_400_BAD_REQUEST)