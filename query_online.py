
from extract_cnn_vgg16_keras import VGGNet

import numpy as np
import h5py

import matplotlib.pyplot as plt
import matplotlib.image as mpimg

from PIL import Image
from io import BytesIO
import base64

#queryImg = Image.open(BytesIO(base64.b64decode(data)))


# read in indexed images' feature vectors and corresponding image names
#h5f = h5py.File(args["index"], 'r')
h5f = h5py.File('./featureCNN.h5', 'r')
feats = h5f['dataset_feat'][:]
imgNames = h5f['dataset_name'][:]
h5f.close()

print("--------------------------------------------------")
print("               searching starts")
print("--------------------------------------------------")

# read and show query image
#queryDir = args["query"]
#queryDir = './database/001_accordion_image_0040.jpg'
queryDir = './dataset-retr/téléchargement.jfif'
queryImg = mpimg.imread(queryDir)
plt.figure()
plt.subplot(2, 1, 1)
plt.imshow(queryImg)
plt.title("Query Image")
plt.axis('off')

# init VGGNet16 model
model = VGGNet()

# extract query image's feature, compute simlarity score and sort
queryVec = model.extract_feat(queryDir)
scores = np.dot(queryVec, feats.T)
rank_ID = np.argsort(scores)[::-1]
rank_score = scores[rank_ID]
print(rank_ID)
print(rank_score)


# number of top retrieved images to show
maxres = 3
imlist = [imgNames[index] for i,index in enumerate(rank_ID[0:maxres])]
imscores = [score for i,score in enumerate(rank_score[0:maxres])]
print("top %d images in order are: " %maxres, imlist)

print(imscores)

maxres = 3
imlist = list()
scores = list(rank_score[0:maxres])
paths = list(rank_ID[0:maxres])
for i in range(maxres):
    result = dict()
    result['path'] = '/dataset-retr/train/' + imgNames[paths[i]].decode('utf-8')
    result['score'] = scores[i]
    imlist.append(result)

print(imlist)
exit(1)

# show top #maxres retrieved result one by one
for i, im in enumerate(imlist):
    image = mpimg.imread('./dataset-retr/train'+"/"+im.decode('UTF-8'))
    plt.subplot(2, 3, i+4)
    plt.imshow(image)
    plt.title("search output %d" % (i + 1))
    plt.axis('off')
plt.show()
