@echo off
rem // Iterate through the immediate sub-directories of the source directory:
for /D %%D in ("C:\Users\thiba\Downloads\CNN_retrieval\CNN_retrieval\dataset-retr\train\Tees_Tanks\*") do (
    rem // Iterate through all files in the currently iterated sub-directory:
    for %%F in ("%%~D\*.*") do (
        rem /* Copy the currently iterated file into the destination directory
        rem    and rename it so that the parent directory name is prefixed: */
        copy /Y "%%~F" "C:\Users\thiba\Downloads\CNN_retrieval\CNN_retrieval\dataset-retr\train\%%~nxD-%%~nxF"
    )
)